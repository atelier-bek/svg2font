# SVG2FONT
A.Gelgon 2017

## requirement
  * python[3/3.6.0 ] 
  * ![python fontforge](https://fontforge.github.io/python.html)

## Documentation

You must put your svg files in `svg/` folder with the good KeyCode. 
(exemple: A -> `svg/65.svg`)

### Execute script
Command-line:

`$ cd existing_folder`

`$ python import.py outPutFileName`
 
 `FINAL/OutputFile.[ttf/oft/sfd]`

## License

GNU General Public License (GPL)
