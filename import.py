# /usr/bin/python

# Importer des librairies python
import sys
import glob
import fontforge

# VARIABLES GLOBALES
# Attribu d'un nom dans le terminale $ python import.py nomDeLaFonte
NAME = sys.argv[1]
# Ouvrir le fichier vide model
FONT = fontforge.open('tmp/empty.sfd')
# Ouvrir la direction avec tout les svg
DIR_SVG = glob.glob('svg/*.svg')
# Motif taille du svg
# LINE_WIDTH = 'width="'

# Boucle qui import les svg dans la fonte
for glyph in DIR_SVG:
	# Ouvrir le svg pour recuperer la valeur wdth = chasse
	svg_file = open(glyph, "r")
	# Boucle qui va lire ligne par ligne le svg
	# for ligne in svg_file:
	# 	# Si il y a le motif LINE_WIDTH
	# 	if LINE_WIDTH in ligne:
	# 		# Creation de la variable avec la valeur du width du svg
	# 		val_width = ligne.split('width="')[-1].split('"')[-2]

	# on recupere le numero unicode dans le chemin
	val_char = glyph.split('/')[-1].split('.')[0]
	# variable d'ouverture du caractere
	letter = FONT.createChar(int(val_char))
	# creer la chasse du caractere
	# letter.width = float(val_width)*1.79
	# importation du svg dans la fonte
	letter.importOutlines(glyph)

	print('lettre -> '+val_char)
# output de la fonte
FONT.generate('FINAL/'+NAME+'.sfd')
FONT.generate('FINAL/'+NAME+'.otf')
FONT.generate('FINAL/'+NAME+'.ttf')
